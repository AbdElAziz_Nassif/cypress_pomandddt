/// <reference types="Cypress" />

/* Designs used in this Framework is Page Object Model and Data Driven Test */
/*importing classes to be used insdie this class */
const { HomePage } = require("../pages/HomePage");
const { FAQPage } = require("../pages/FAQPage");
const { AssertResults } = require("../assertions/Assertions");

/*taking Objects of the classes to be used insdie this class */
const homeObj = new HomePage();
const faqPage = new FAQPage();
const assertTest = new AssertResults();


describe("FAQ Suite", () => {
    const numberOfQuestions = 7;
    var questions_testData = new Set();
    
    beforeEach(() => {
        cy.visit('/faq');
        
        cy.fixture('FAQ_data').as('data');
        cy.get('@data').then((testData) => {
            questions_testData.add(testData.Quest_001)
            .add(testData.Quest_002)
            .add(testData.Quest_003)
            .add(testData.Quest_004)
            .add(testData.Quest_005)
            .add(testData.Quest_006)
            .add(testData.Quest_007);
        })
    })
    it('Validate Questions numbers, Questions contnet & page title of FAQ page test case', () => {
        faqPage.getFaqPageTitle().should('be.visible');
        var numberOfQuestions_local = faqPage.checkTheCountOfQuestionsAndAnswers();
        numberOfQuestions_local.should('have.length', numberOfQuestions);
        const questionsIterator = questions_testData.values();
        for(var i=1;i<=7;i++)
        {
            var que_local = faqPage.checkQuestionContent(i);
            que_local.should('have.text', questionsIterator.next().value);
        }
    })
})
