/// <reference types="Cypress" />

/*importing classes to be used insdie this class */
import { PageBase } from "../cypressWrapper/PageBase";
import { LocatorBase } from "../cypressWrapper/LocatorBase";
const { AssertResults } = require("../assertions/Assertions");
require('cypress-xpath')

/*taking Objects of the classes to be used insdie this class */
const locator = new LocatorBase();

/*Web Elements locators to be used in the FAQ page*/
var FaqPageTitle_Locator = "h1[class='css-bm95t9 e19vwfi40']";

var all_ques_Locator = "//section[@class='css-13dahp2 e19vwfi43']//div[@class='Collapsible']";


/*this class will be used as test steps and asserions generator for the Test cases */
export class FAQPage {
	/*function to check the number of questions in the FAQ page */
	checkTheCountOfQuestionsAndAnswers()
	{
		var numberOfQues = cy.xpath(all_ques_Locator);
		return numberOfQues;
	}
	/*function to check the header of the FAQ page */
	getFaqPageTitle()
	{
		var pageTitle = locator.locateByFullCss(FaqPageTitle_Locator);
		return pageTitle;
	}
	
	/*Index is 1-based to get adapted with the locator index*/ 
	checkQuestionContent(questionIndex)
	{
		var que = cy.get(':nth-child('+ questionIndex + ') > .Collapsible__trigger > .css-olkrgj');
		que.scrollIntoView();
		return que;
	}

} 